﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		while (true) {
			Scanner tastatur = new Scanner(System.in);

			double zuZahlenderBetrag;
			double eingezahlterGesamtbetrag;
			double eingeworfeneMünze;
			double rückgabebetrag;
			int anzahlTicket;
			String[] FahrscheinName;
			double[] FahrscheinPreise;

			zuZahlenderBetrag = farkartenBestellungErfassen();

			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
			
		}
	}

	public static double farkartenBestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		
		double zuZahlenderBetrag = 0;
		
		int FahrscheinAuswahl = 0;
		double[] FahrscheinPreise = {0,2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
		String[] FahrscheinName = {"--------------------","|1.    Einzelfahrschein Berlin AB","|2.    Einzelfahrschein Berlin BC","|3.    Einzelfahrschein Berlin ABC","|4.    Kurzstrecke","|5.    Tageskarte Berlin AB",
				"|6.    Tageskarte Berlin BC","|7.    Tageskarte Berlin ABC","|8.    Kleingruppen-Tageskarte Berlin AB","|9.    Kleingruppen-Tageskarte Berlin BC","|10.   Kleingruppen-Tageskarte Berlin ABC"};
		System.out.printf("%-60s %10s \n","Fahrscheinbezeichnung:", "Kosten:");
		System.out.println("-----------------------------------------------------------------------");
		ArrayAusgeben(FahrscheinName,FahrscheinPreise);
		System.out.println("\nEingabe der Fahrscheinnummer:");
		
		int auswahl = tastatur.nextInt();
		int k = auswahl;
		
		zuZahlenderBetrag = FahrscheinPreise[k];
		

		int anzahlTicket = 0;
		System.out.print("Ihre Anzahl der Tickets: ");
		while (anzahlTicket == 0) {
			anzahlTicket = tastatur.nextInt();
			if (anzahlTicket >= 11 || anzahlTicket < 1) {
				System.out.println("ERROR!!! Eingabe einer Zahl zwischen 1 und 10 verlangt");
				anzahlTicket = 0;
			} else {
				anzahlTicket = anzahlTicket;

			}
		}
		zuZahlenderBetrag = zuZahlenderBetrag * anzahlTicket;
		return zuZahlenderBetrag;
		

	}
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double bezahlterBetrag = 0.0;

		while (bezahlterBetrag < zuZahlenderBetrag) {
			double ausstehendeBetrage = zuZahlenderBetrag;
			ausstehendeBetrage = ausstehendeBetrage - bezahlterBetrag;
			System.out.printf("Noch zu zahlen (€): %.2f €\n", ausstehendeBetrage);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			bezahlterBetrag += eingeworfeneMünze;
		}

		return bezahlterBetrag;
	}

	public static void fahrkartenAusgeben() {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("\nFahrschein wird gedruckt, einen Augenblick gedult");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}
	
	
	public static void ArrayAusgeben (String[] FahrscheinName,double[] FahrscheinPreis) {
		for(int i = 1 ; i < FahrscheinName.length; i++) {
			System.out.printf("%-60s %10s \n",FahrscheinName[i],FahrscheinPreis[i]+ "0€");
		}
		
	}
	

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + "0€");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
			while (rückgabebetrag >= 0.02)// 2 CENT-Münzen
			{
				System.out.println("2 CENT");
				rückgabebetrag -= 0.02;
			}
			while (rückgabebetrag >= 0.01)// 1 CENT-Münzen
			{
				System.out.println("1 CENT");
				rückgabebetrag -= 0.01;
			}
			
		}
		

		System.out.println("\nDen Fahrschein nicht vergessen!! :) \n" + "bevor Sie Strafe zahlen müssen bitte die Fahrkarte entwerten ;)!!\n"
				+ "Tschüssi eine angenehme Fahrt\n");
		

	}
	

}